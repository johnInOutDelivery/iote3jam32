﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DashboardDetails.aspx.cs" Inherits="LookEnjoy.DashboardDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style>

        .container{
            width:1000px;
            height:AUTO;
        }

        .box{
           width:200px;
           height:200px;
           float:left;
           margin: 1em;
           text-align:center;
           -webkit-box-shadow: 3px 1px 12px 0px rgba(50, 50, 50, 0.46);
            -moz-box-shadow:    3px 1px 12px 0px rgba(50, 50, 50, 0.46);
            box-shadow:         3px 1px 12px 0px rgba(50, 50, 50, 0.46)
        }

        .imgCenter{
            margin-top:25%;
        }
        img{
            width:200px;
            margin-top: -46px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div style="text-align:center">
        <div class="container">
            <asp:PlaceHolder runat="server" ID="pclID"></asp:PlaceHolder>
        </div>
        <p></p>
        <div style="clear:both"></div>

        <div class="container">
            <asp:PlaceHolder runat="server" ID="pclIDSuggestion"></asp:PlaceHolder>
        </div>
    </div>
    </form>
</body>
</html>
