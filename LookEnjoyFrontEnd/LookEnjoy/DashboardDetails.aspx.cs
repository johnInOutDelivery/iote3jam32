﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace LookEnjoy
{
    public partial class DashboardDetails : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string id = Request.QueryString["id"];
            GetConsumerID(id);
        }

        public void GetConsumerID(string id)
        {
            var urlID = "http://10.97.21.147:8080/api/Consumers/" + id;
            var urlProducts = "http://10.97.21.147:8080/api/Products/";

            using (WebClient wc = new WebClient())
            {
                //Mostra o usuário escolhido
                var json = wc.DownloadString(urlID);
                var consumers = JsonConvert.DeserializeObject<Consumers>(json);
                var _age = "Não identificado";
                var _gender = "Não identificado";
                

                HtmlGenericControl createDiv = new HtmlGenericControl("DIV");
                createDiv.ID = "createDiv";
                createDiv.InnerHtml += "<table><tr><td>";
                createDiv.InnerHtml += "<div class='box'><div style='margin-top:20%;'><img src='" + consumers.image + "' width='100px' /></div></div>";
                createDiv.InnerHtml += "</td>";

                if (consumers.age != null)
                    _age = consumers.age;

                if (consumers.gender != null)
                    _gender = consumers.gender.Replace("Male", "Masculino").Replace("Female", "Feminino");

                createDiv.InnerHtml += "<td>";
                createDiv.InnerHtml += "<div style='float:left'><span style='font-size:18pt'>Idade:</span> " + _age + "</div><div style='clear:both'/><br><br><span style='font-size:18pt'>Sexo:</span> " + _gender;
                createDiv.InnerHtml += "</td>";
                createDiv.InnerHtml += "</tr></table>";

                pclID.Controls.Add(createDiv);


                //Mostra as sugestões
                var jsonProducts = wc.DownloadString(urlProducts);
                var productsToID = JsonConvert.DeserializeObject<Consumers[]>(jsonProducts);
                foreach (var cs in productsToID)
                {
                    if(_gender != null)
                    {
                        if (cs.gender.ToLower() == _gender.ToLower())
                        {
                            HtmlGenericControl createDiv2 = new HtmlGenericControl("DIV");
                            createDiv2.ID = "createDiv";
                            createDiv2.InnerHtml = "<div class='box' ><div class='imgCenter'><img src='" + cs.imagem + "' style='width: 200px; '/></div></div>";
                            pclIDSuggestion.Controls.Add(createDiv2);
                        }
                    }
                    else
                    {
                        if (cs.gender.ToLower() == "mascolino")
                        {
                            HtmlGenericControl createDiv2 = new HtmlGenericControl("DIV");
                            createDiv2.ID = "createDiv";
                            createDiv2.InnerHtml = "<div class='box' ><div class='imgCenter'><img src='" + cs.imagem + "' style='width: 200px; '/></div></div>";
                            pclIDSuggestion.Controls.Add(createDiv2);
                        }
                    }
                    
                }
            }
        }

        public class Consumers
        {
            public string age { get; set; }
            public string gender { get; set; }
            public string imagem { get; set; }
            public string image { get; set; }
            public string id { get; set; }
            public string name { get; set; }
        }
    }
}