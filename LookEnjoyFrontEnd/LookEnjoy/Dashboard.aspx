﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="LookEnjoy.Dashboard" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <%--<meta http-equiv="refresh" content="5" >--%>

    <style>
        #container{
            width:1000px;
            height:AUTO;
        }
        .box{
           width:200px;
           height:200px;
            -webkit-box-shadow: 3px 1px 12px 0px rgba(50, 50, 50, 0.46);
            -moz-box-shadow:    3px 1px 12px 0px rgba(50, 50, 50, 0.46);
            box-shadow:         3px 1px 12px 0px rgba(50, 50, 50, 0.46);
           float:left;
           margin: 1em;
           text-align:center;
        }
        img{
            width:200px;
            margin-top: -40px;
        }

        .imgCenter{
            margin-top:20%;
        }

        .box:hover{
        background: blue; /* make this whatever you want */
        }
    </style>

</head>
<body>
    <form id="form1" runat="server">

    <div id="container">
        <asp:PlaceHolder runat="server" ID="pclDiv"></asp:PlaceHolder>
        <%--<div class="box"><div class="imgCenter" onclick="javascript: document.location.href='DashboardDetails.aspx'"><img src="Images/imgFoto.png" /></div></div>
        <div class="box"><div class="imgCenter"><img src="Images/imgFoto.png" /></div></div>
        <div class="box"><div class="imgCenter"><img src="Images/imgFoto.png" /></div></div>
        <div class="box"><div class="imgCenter"><img src="Images/imgFoto.png" /></div></div>
        <div class="box"><div class="imgCenter"><img src="Images/imgFoto.png" /></div></div>--%>
    </div>

    </form>
</body>
</html>
