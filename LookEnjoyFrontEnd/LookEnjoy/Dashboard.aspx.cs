﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace LookEnjoy
{
    public partial class Dashboard : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //string json = @"{
            //                  'age': '10',
            //                  'gender': '1995-4-7T00:00:00',
            //                  'image': '1995-4-7T00:00:00',
            //                }";

            //Consumers m = JsonConvert.DeserializeObject<Consumers>(json);
            //string name = m.age;
            

            GetConsumers();
        }

        public void GetConsumers()
        {
            try
            {
                var url = "http://10.97.21.147:8080/api/Consumers";

                using (WebClient wc = new WebClient())
                {
                    var json = wc.DownloadString(url);
                    var consumers = JsonConvert.DeserializeObject<Consumers[]>(json);

                    foreach (var cs in consumers.OrderByDescending(t => t.image))
                    {

                        HtmlGenericControl createDiv = new HtmlGenericControl("DIV");
                        createDiv.ID = "createDiv";
                        createDiv.InnerHtml = "<div class='box'><div class='imgCenter' onclick='javascript: document.location.href = \"DashboardDetails.aspx?id=" + cs.id + "\"'><img src='" + cs.image + "' /></div></div>";
                        pclDiv.Controls.Add(createDiv);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public class Consumers
        {
            public string age { get; set; }
            public string gender { get; set; }
            public string image { get; set; }
            public string id { get; set; }
        }

    }
}