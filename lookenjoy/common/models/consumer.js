var AWS = require('aws-sdk');
AWS.config.update({region:'us-east-1'});

module.exports = function (Consumer) {
    Consumer.rekognition = function (req, cb) {
        var rekognition = new AWS.Rekognition();
        var url_test = "01.png"
        var url = req.url;
        var params = url.substring(url.indexOf('?')+1);
        var name = params.split('=');
        var image_url = "https://s3.amazonaws.com/lookenjoy/"+name[1];
        var Gender = {
          "Value": "Male",
          "Confidence": 99.92910766601562
        }
        var AgeRange = {
          "Low": 26,
          "High": 43
        }
        var Emotions = [
          {
            "Type": "HAPPY",
            "Confidence": 86.57554626464844
          },
          {
            "Type": "CONFUSED",
            "Confidence": 17.026721954345703
          },
          {
            "Type": "SURPRISED",
            "Confidence": 1.8225431442260742
          }
        ]

        var params = {
          Image: {
            S3Object: {
              Bucket: "lookenjoy",
              Name: name[1]
            }
          },
          Attributes: [
            "ALL"
          ]
        };

        rekognition.detectFaces(params, function(err, data) {
          if (err) console.log(err, err.stack); // an error occurred
          else{
            if(data.FaceDetails){
              var str = JSON.stringify(data.FaceDetails, null, 2)
              console.log(str)
              if(data.FaceDetails.length>0){
                var face = data.FaceDetails[0];

                if(face.Gender == null){
                  console.log("is null")
                }

                Gender = face.Gender

                AgeRange = face.AgeRange

                Emotions = face.Emotions

                console.log(str)

                console.log(Gender)
                console.log(AgeRange)
                console.log(Emotions)

                var consumer = Consumer.create({name: 'Usuario', gender: Gender.Value, age: AgeRange.Low+" - "+AgeRange.High, emotion: Emotions[0].Type, image: image_url})

              }
            }
          }
        })

        cb(null, name);
    };

    Consumer.image = function(cb){
      var url = "https://s3.amazonaws.com/lookenjoy/02be7060-cc5e-4ab8-a6ff-63a607d16030.jpg"
      cb(null, url);
    }

    Consumer.remoteMethod('rekognition', {
        http: {
            path: '/rekognition',
            verb: 'get'
        },
        accepts: {
          arg: 'req',
          type: 'object',
          'http': {source: 'req'}
        },
        returns: {
            arg: 'status',
            type: 'string'
        }
    });

    Consumer.remoteMethod('image', {
      http: {
        path: '/image',
        verb: 'get'
      },
      returns: {
        arg: 'image',
        type: 'string'
      }
    });
};
