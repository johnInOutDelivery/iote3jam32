var AWS = require('aws-sdk');
var uuid = require('node-uuid');

module.exports = function(Consumer) {
  Consumer.rekognition = function(cb) {
    var s3 = new AWS.S3();
    var url = "http://media2.s-nbcnews.com/i/newscms/2016_05/960051/today-jennifer-lopez-tease-160204_8461450760118829b2d32eeb6d1dd9cc.jpg"
    var response = 'File name: ';

    console.log(s3)

    cb(null, response);
  };
  Consumer.remoteMethod(
    'rekognition', {
      http: {
        path: '/rekognition',
        verb: 'get'
      },
      returns: {
        arg: 'status',
        type: 'string'
      }
    }
  );

};
