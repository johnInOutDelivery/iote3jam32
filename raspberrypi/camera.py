#Libraries
import RPi.GPIO as GPIO
import time
import subprocess
import uuid
import sys
import boto3
import requests

AWS_ACCESS_KEY_ID='AKIAJKPBV7BP2MYGRFIQ'
AWS_SECRET_ACCESS_KEY='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'

#GPIO Mode (BOARD / BCM)
GPIO.setmode(GPIO.BCM)

#set GPIO Pins
GPIO_TRIGGER = 18
GPIO_ECHO = 24

#set GPIO direction (IN / OUT)
GPIO.setup(GPIO_TRIGGER, GPIO.OUT)
GPIO.setup(GPIO_ECHO, GPIO.IN)

def distance():
    # set Trigger to HIGH
    GPIO.output(GPIO_TRIGGER, True)

    # set Trigger after 0.01ms to LOW
    time.sleep(0.00005)
    GPIO.output(GPIO_TRIGGER, False)

    StartTime = time.time()
    StopTime = time.time()

    # save StartTime
    while GPIO.input(GPIO_ECHO) == 0:
        StartTime = time.time()


    # save time of arrival
    while GPIO.input(GPIO_ECHO) == 1:
        StopTime = time.time()

    # time difference between start and arrival
    TimeElapsed = StopTime - StartTime
    # multiply with the sonic speed (34300 cm/s)
    # and divide by 2, because there and back
    distance = (TimeElapsed * 34300) / 2

    return distance

def sendS3(path,filename):
    s3=boto3.resource('s3',aws_access_key_id=AWS_ACCESS_KEY_ID,aws_secret_access_key= AWS_SECRET_ACCESS_KEY)
    s3.meta.client.upload_file(path,'lookenjoy',filename)
    print(filename)
    r = requests.get('http://10.97.21.147:8080/api/Consumers/rekognition?nome=' + filename)
    print(r)
    

if __name__ == '__main__':
    try:
        while True:
	    count=0
	    for x in range(0, 10):
                dist = distance()
                print ("Measured Distance = %.1f cm" % dist)
	        if dist <= 40:
		    count += 1
	    print("Tirou as 10 fotos,count: %d" % count)
            if count >= 8 :
                print("Tirando foto")
                filename=str(uuid.uuid4())+".png"
                path="/home/pi/Desktop/lookenjoy/"+filename
                
                process=subprocess.Popen(["raspistill","-o",path,"-w","400","-h","400","-q","100","-e","png"],stdout=subprocess.PIPE)
                process.communicate()
                print("Foto Tirada")
                print("Enviando para o S3")
                sendS3(path,filename)
                time.sleep(7)
   # Reset by pressing CTRL + C
    except KeyboardInterrupt:
        print("Measurement stopped by User")
        GPIO.cleanup()
